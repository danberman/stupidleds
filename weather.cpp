#include "weather.h"
#include <ESP8266WiFi.h>

WiFiClient client;
  
int Weather::requestUpdate(String apiKey, double lat, double lon) {
  const int httpPort = 80;
  if (!client.connect("217.26.50.8", httpPort)) {
    return -1;
  }
  
  // We now create a URI for the request
  String url = "/rest/weather?apiKey=" + apiKey + "&lat=" + String(lat) + "&lon=" + String(lon) + "&units=" + myUnits;
  
  // This will send the request to the server
  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: www.squix.org\r\n" +                
               "Connection: close\r\n\r\n");
  return 0;
}

bool Weather::updateAvailable(void) {
  return client.available();
}

void Weather::doUpdate(void) {
  while(client.available()){
    String line = client.readStringUntil('\n');
    String key = getKey(line);
    if (key.length() > 0) {
        if (key=="DATE") { 
          parseDate(line);
          return;
        }
        String value = getValue(line);
        if (key=="CURRENT_TEMP") {
         currentTemp = value.toInt();
        } else if(key =="CURRENT_HUMIDITY") {
         currentHumidity = value.toInt();
        } else if (key =="CURRENT_ICON") {
         currentIcon = value;
        } else if (key =="CURRENT_SUMMARY") {
         currentSummary = value;
        } else if (key =="MAX_TEMP_TODAY") {
         maxTempToday = value.toInt();
        } else if (key =="MIN_TEMP_TODAY") {
         minTempToday = value.toInt();
        } else if (key =="ICON_TODAY") {
         iconToday = value;
        } else if (key =="SUMMARY_TODAY") {
         summaryToday = value;
        } else if (key =="MAX_TEMP_TOMORROW") {
         maxTempTomorrow = value.toInt();
        } else if (key =="ICON_TOMORROW") {
         iconTomorrow = value;
        } else if (key =="MIN_TEMP_TOMORROW") {
         minTempTomorrow = value.toInt();
        } else if (key =="SUMMARY_TODAY") {
         summaryTomorrow = value;
        }
    } 
  }  
}

void Weather::setUnits(String units) {
   myUnits = units; 
}

String Weather::getKey(String line) {
  if (line.startsWith("Date")) {
    return "DATE";
  }
  int separatorPosition = line.indexOf("=");
  if (separatorPosition == -1) {
    return "";
  }  
  return line.substring(0, separatorPosition);
}

String Weather::getValue(String line) {
  int separatorPosition = line.indexOf("=");
  if (separatorPosition == -1) {
    return "";
  }  
  return line.substring(separatorPosition + 1);
}

void Weather::parseDate(String line) {
  timestr = line;
  weekday = line.substring(6,9);
  day = line.substring(11,13).toInt();
  month = line.substring(14,17);
  year = line.substring(18,22).toInt();
  hour = line.substring(23,25).toInt();
  minute = line.substring(26,28).toInt();
  second = line.substring(29,31).toInt();
  timezone = line.substring(32,36).toInt();
}

String Weather::getTimeStr(void) {
  return timestr;
}

int Weather::getHour(void) {
  return hour;
}
int Weather::getMinute(void) {
  return minute;
}
int Weather::getCurrentTemp(void) {
  return currentTemp;
}
int Weather::getCurrentHumidity(void) {
  return currentHumidity;
}
String Weather::getCurrentIcon(void) {
  return currentIcon;
}
String Weather::getCurrentSummary(void) {
  return currentSummary;
}
String Weather::getIconToday(void) {
  return iconToday;
}
int Weather::getMaxTempToday(void) {
  return maxTempToday;
}
int Weather::getMinTempToday(void) {
  return minTempToday;
}
String Weather::getSummaryToday(void) {
  return summaryToday;
}
int Weather::getMaxTempTomorrow(void) {
  return maxTempTomorrow;
}
int Weather::getMinTempTomorrow(void) {
  return minTempTomorrow;
}
String Weather::getIconTomorrow(void) {
  return iconTomorrow;
}
String Weather::getSummaryTomorrow(void) {
  return summaryTomorrow;
}
