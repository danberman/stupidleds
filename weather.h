#ifndef WEATHER_H
#define WEATHER_H

#include <Arduino.h>

class Weather {
  private:
    int data_valid;
    int currentTemp;
    int currentHumidity;
    String currentIcon;
    String currentSummary;
    String iconToday;
    int maxTempToday;
    int minTempToday;
    String summaryToday;
    int maxTempTomorrow;
    int minTempTomorrow;
    String iconTomorrow;
    String summaryTomorrow;
    String apiKey;
    String myUnits = "us";
    String myLanguage;

    String timestr;
    String weekday;
    int day;
    String month;
    int year;
    int hour;
    int minute;
    int second;
    String timezone;
    
    String getValue(String line);
    String getKey(String line);
    void parseDate(String line);
  
  public:
    int requestUpdate(String apiKey, double lat, double lon);
    bool updateAvailable(void);
    void doUpdate(void);
    void setUnits(String units);
    int getCurrentTemp(void);
    int getCurrentHumidity(void);
    String getCurrentIcon(void);
    String getCurrentSummary(void);
    String getIconToday(void);
    int getMaxTempToday(void);
    int getMinTempToday(void);
    String getSummaryToday(void);
    int getMaxTempTomorrow(void);
    int getMinTempTomorrow(void);
    String getIconTomorrow(void);
    String getSummaryTomorrow(void);
    int getHour(void);
    int getMinute(void);
    String getTimeStr(void);
};

#endif
