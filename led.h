#ifndef LED_H
#define LED_H

const float red_scale = 1.0;
const float green_scale = 1.0;
const float blue_scale = 1.0;

class LED {
  public:
  void led_init(void);
  void set_brightness(float led_brightness);
  uint32_t by_colormap(uint8_t const colormap[][3], uint8_t progress);
  void set_color_rgb(uint8_t rgb[3]);
  void set_color_hex(uint32_t hex);
  uint32_t fade_rgb(uint8_t const start_color[3], uint8_t const end_color[3], uint8_t progress);
  uint32_t fade_hex(uint32_t start_color, uint32_t end_color, uint16_t progress);
  void hex_to_rgb(uint32_t hex, uint8_t rgb[3]);
  uint32_t rgb_to_hex(uint8_t const rgb[3]);
};

#endif
