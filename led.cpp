#include <Arduino.h>
#include "led.h"
extern "C" {
  #include "pwm.h"
}

#define BLUE_LED 12
#define GREEN_LED 13
#define RED_LED 14
#define BOARD_LED 4

#define BLUE_PWM 0
#define GREEN_PWM 1
#define RED_PWM 2

#define PWM_MAX 600

static float brightness = 0.3;

void LED::led_init() {
  uint32 pwm_duty_init[3] = {0, 0, 0};
  uint32 io_info[3][3] = {
  {PERIPHS_IO_MUX_MTDI_U, FUNC_GPIO12, 12},
  {PERIPHS_IO_MUX_MTCK_U, FUNC_GPIO13, 13},
  {PERIPHS_IO_MUX_MTMS_U, FUNC_GPIO14, 14}
  };
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
  pinMode(14, OUTPUT);
  pwm_init(PWM_MAX, pwm_duty_init, 3, io_info);
  pwm_start();
}

void LED::set_brightness(float led_brightness) {
  brightness = led_brightness;
}

void LED::set_color_rgb(uint8_t rgb[3]) {
  float const scale = (PWM_MAX * brightness)/255.0;
  uint16_t r = rgb[0] * scale * red_scale;
  uint16_t g = rgb[1] * scale * green_scale;
  uint16_t b = rgb[2] * scale * blue_scale;
  pwm_set_duty(r, RED_PWM);
  pwm_set_duty(b, BLUE_PWM);
  pwm_set_duty(g, GREEN_PWM);
  pwm_start();
}

void LED::set_color_hex(uint32_t hex) {
  uint8_t rgb[3];
  rgb[0] = (hex & 0xFF0000) >> 16;
  rgb[1] = (hex & 0x00FF00) >>  8;
  rgb[2] = (hex & 0x0000FF);
  set_color_rgb(rgb);
}

void LED::hex_to_rgb(uint32_t hex, uint8_t rgb[3]) {
  rgb[0] = (uint16_t)((hex & 0xFF0000) >> 16);
  rgb[1] = (uint16_t)((hex & 0x00FF00) >>  8);
  rgb[2] = (uint16_t)(hex & 0x0000FF);
}

uint32_t LED::rgb_to_hex(uint8_t const rgb[3]) {
  return (rgb[0] << 16) | (rgb[1] << 8) | rgb[2];
}

uint32_t LED::by_colormap(uint8_t const colormap[][3], uint8_t progress) {
  return rgb_to_hex(colormap[progress]);
}

/** @brief fade between two colors
 *  @param[in] start_color starting color as RGB array
 *  @param[in] end_color end color as RGB array
 *  @param[in] progress fade between colors {0-255}
 */
uint32_t LED::fade_rgb(uint8_t const start_color[3], uint8_t const end_color[3], uint8_t progress) {
  uint8_t out[3];
  for(uint8_t ii = 0; ii < sizeof(start_color)/sizeof(start_color[0]); ++ii) {
    out[ii] = (((int32_t)end_color[ii] - (int32_t)start_color[ii]) * progress)/300 + (int32_t)start_color[ii];
  }
  return rgb_to_hex(out);
}

uint32_t LED::fade_hex(uint32_t start_color, uint32_t end_color, uint16_t progress) {
  uint8_t start_rgb[3];
  uint8_t end_rgb[3];
  hex_to_rgb(start_color, start_rgb);
  hex_to_rgb(end_color, end_rgb);
  return fade_rgb(start_rgb, end_rgb, progress);
}

