#ifndef CONFIG_H
#define CONFIG_H

const char* ssid = "HamiltonBeachFoodProcessor";
const char* password = "bigturtlesarefun";

String apiKey = "b79f1f0203c10c3bb6fd1b32eaf246ba";
float lat = 42.402904;
float lon = -71.108504;

const uint32_t timer_period = 10; /* ms */

const uint32_t weather_update_freq = 1500000; /* ms */

const uint32_t red = 0xFF0000;
const uint32_t green = 0x00FF00;
const uint32_t blue = 0x0000FF;
const uint32_t yellow = 0xFFCC00;
const uint32_t orange = 0xFF1900;
const uint32_t pink = 0xFF990A;
const uint32_t purple = 0xFF0040;
const uint32_t cyan = 0x00FFC8;
const uint32_t white = 0xFFFFFF;
const uint32_t grey = 0xCCCCCC;

const int32_t cold_temp = 30;
const int32_t hot_temp = 90;

const uint32_t snow_color = white;
const uint32_t rain_color = green;
const uint32_t clear_color = yellow;
const uint32_t cloudy_color = purple;
const uint32_t wind_color = pink;
const uint32_t fog_color = purple;

const uint16_t max_led_period = 50;

const uint32_t party_trans_period = 100;
const uint32_t party_brightness = 1.0;

const uint32_t white_light_color = 0xFFFFFFFF;
const float white_light_brightness = 1.0;

const float hourly_brightness_scale = 0.2;

/* Note: Ordered In GMT: EST + 5 */
const float brightness_by_hour[24] = { 
  .9, // 19 
  .8, // 20
  .5, // 21
  .3, // 22
  .2, // 23
  .1, // 0
  .1, // 1
  .1, // 2
  .1, // 3
  .1, // 4
  .1, // 5
  .2, // 6
  .3, // 7
  .4, // 8
  .5, // 9
  .6, // 10
  .7, // 11
   1, // 12
   1, // 13
   1, // 14
   1, // 15
   1, // 16
   1, // 17
   1, // 18
};
  
const uint32_t weather_periods[4] = {5000, 2000, 3000, 2000};

#endif
