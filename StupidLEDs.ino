#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WebSocketsServer.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <math.h>

#include "user_interface.h"

#include "weather.h"
#include "led.h"
#include "config.h"
#include "ota.h"
#include "colormaps.h"
#include "webpage.h"

ESP8266WebServer server(80);
WebSocketsServer webSocket = WebSocketsServer(81);
Weather weather;
LED led;
MDNSResponder mdns;
static bool do_update_immediately = true;

static os_timer_t myTimer;
static uint32_t ms = 0;

static uint32_t temp_color;
static uint32_t icon_color;

enum {
  WEATHER,
  WHITE_LIGHT,
  PARTY
};

static int curr_state = WEATHER;
static void set_state(int state) {
   curr_state = state;
   if (curr_state == WEATHER) {
      do_update_immediately = true;
   }
}

static void update_state(void) {
  switch(curr_state) {
    case WHITE_LIGHT:
      led.set_color_hex(white_light_color);
      led.set_brightness(white_light_brightness);
      break;
    case WEATHER:
    {
      enum {
        TEMP,
        FADE_TO_ICON,
        ICON,
        FADE_TO_TEMP,
        WEATHER_STATE_END,
      };
      static uint32_t last_change = 0;
      static int weather_state = TEMP;
      static uint32_t period = weather_periods[weather_state];
      uint8_t progress; /* 0 - 255 indicator of state */
      if((ms - last_change) > period) {
        weather_state = (weather_state + 1) % WEATHER_STATE_END;
        last_change = ms;
        period = weather_periods[weather_state];
        progress = 0;
      }
      else {
        progress = ((ms - last_change) * 255)/period;
      }
      bool updated = false;
      float w = (2*PI) * (progress/255.0);
      float brightness = .5 * cos(w) + .5;
      uint32_t color;
      brightness = constrain(brightness, .07, 1.0);
      switch(weather_state) {
        case TEMP:
            break;
        case ICON:
            break;
        case FADE_TO_ICON:
          color = led.fade_hex(temp_color, icon_color, progress);
          updated = true;
          break;
        case FADE_TO_TEMP:
          color = led.fade_hex(icon_color, temp_color, progress);
          updated = true;
          break;
      }
      if(updated) {
        led.set_color_hex(color);
        led.set_brightness(brightness * hourly_brightness_scale * brightness_by_hour[weather.getHour()]);
      }
      break;
    }
    case PARTY:
      if (ms % party_trans_period == 0) {
         uint8_t rgb[3];
         for(uint8_t ii = 0; ii<3; ++ii) {
          rgb[ii] = random(0,255);
         }
         led.set_color_rgb(rgb);
         led.set_brightness(party_brightness);
      }
      break;
  }
}

void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t lenght) {
  switch(type) {
    case WStype_DISCONNECTED:    
    break;
    case WStype_CONNECTED:
    {
      webSocket.sendTXT(num, "Connected"); 
    }
    break;
    case WStype_TEXT:
    {
      
      if (strcmp(white_light_str, (const char *)payload) == 0) {
        set_state(WHITE_LIGHT);
        webSocket.sendTXT(num, "White light");
      }
      else if (strcmp(party_str, (const char *)payload) == 0) {
        set_state(PARTY);   
        webSocket.sendTXT(num, "Party");  
      }
      else {
        set_state(WEATHER);
        webSocket.sendTXT(num, "Weather");
        char buf[50];
        weather.getCurrentIcon().toCharArray(buf,50);
        webSocket.sendTXT(num, buf);
        String(weather.getCurrentTemp()).toCharArray(buf,50);
        webSocket.sendTXT(num, buf);
        String(weather.getTimeStr()).toCharArray(buf,50);
        webSocket.sendTXT(num, buf);
      }
    }
  }
}

static void handle_root(void) {
    server.send_P(200, "text/html", INDEX_HTML);
}

void webserver_init(void) {
  server.on("/", handle_root);
  server.begin();
}

void mdns_init(void) {
  if (mdns.begin("WebSock", WiFi.localIP())) {
    mdns.addService("http", "tcp", 80);
    mdns.addService("ws", "tcp", 81);
  }
}

void websocket_init(void) {
    webSocket.begin();
    webSocket.onEvent(webSocketEvent);
}

static void timer_callback(void *pArg) {
  ms += timer_period;
}

void timer_init(void) {
  os_timer_setfn(&myTimer, timer_callback, NULL);
  os_timer_arm(&myTimer, timer_period, true);
}

int wifi_init(void) {
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(500);
    ESP.restart();
  }
  return 0;
}

void setup() {
  Serial.begin(115200);
  Serial.println("Booting");
  wifi_init();
  ota_init();
  mdns_init();
  websocket_init();
  webserver_init();
  
  
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  timer_init();
  led.led_init();
  led.set_color_hex(0x000000);
}

void serial_input(void) {
  int const BUF_SIZE = 20;
  static char buf[BUF_SIZE];
  static char ind = 0;
  int next = Serial.read();
  if(next < 0) {
    return;
  }
  if(next == '\n') {
    buf[ind] = '\0';
    switch(buf[0]) {
      case 'r':
        ind = 0;
        break;
      case 'g':
        ind = 1;
        break;
      case 'b':
        ind = 2;
        break;
      default:
        return;
    }
    ind = 0;
  }
  else {
    buf[ind] = (char)next;
    ++ind;
    if(ind >= BUF_SIZE) {
      ind = 0;
    }
  }
}

static void update_color(void) {
  int32_t temp = weather.getCurrentTemp();
  int32_t temp_grad = ((temp-cold_temp) * 255) / (hot_temp - cold_temp);
  temp_grad = constrain(temp_grad, 0, 255);
  temp_color = led.by_colormap(cmap_jet, temp_grad);
  
  String icon = weather.getIconToday();
  if(icon == "snow") icon_color = snow_color;
  else if(icon == "rain") icon_color = rain_color;
  else if(icon == "wind") icon_color = wind_color;
  else if(icon == "fog") icon_color = fog_color;
  else if(icon == "cloudy") icon_color = cloudy_color;
  else if(icon == "partly-cloudy-day") icon_color = cloudy_color;
  else if(icon == "partly-cloudy-night") icon_color = cloudy_color;
  else if(icon == "clear-day") icon_color = clear_color;
  else if(icon == "clear_night") icon_color = clear_color;
  else if(icon == "sleet") icon_color = snow_color;
  else {
    icon_color = cloudy_color; /* Seems like a safe default... */
  }
}

void loop() {
  uint32_t start_ms = ms;
  if(ms % weather_update_freq == 0 || do_update_immediately) {
    weather.requestUpdate(apiKey,lat,lon);
    do_update_immediately = false;
  }
  if( ms - start_ms < max_led_period) { /* Force LED updates */
    if(weather.updateAvailable()) {
      weather.doUpdate();
      update_color();
    }
  }
  ota_handle();
  update_state();
  webSocket.loop();
  server.handleClient();
}
