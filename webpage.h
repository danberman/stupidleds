const char party_str[] = "party";
const char white_light_str[] = "white";
const char weather_str[] = "weather";

const char PROGMEM INDEX_HTML[] = R"rawliteral(
<!DOCTYPE html>
<html>
<head>
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0, maximum-scale = 1.0, user-scalable=0">
<title>The Stupidest LEDs</title>
<style>
"body { background-color: #808080; font-family: Arial, Helvetica, Sans-Serif; Color: #000000; }"
</style>
<script>
var websock;
function start() {
  websock = new WebSocket('ws://' + window.location.hostname + ':81/');
  websock.onopen = function(evt) { console.log('websock open'); };
  websock.onclose = function(evt) { console.log('websock close'); };
  websock.onerror = function(evt) { console.log(evt); };
  websock.onmessage = function(evt) {
    console.log(evt);
    var e = document.getElementById('ledstatus');
    if (evt.data === 'party') {
      e.style.color = 'red';
    }
    else if (evt.data === 'weather') {
      e.style.color = 'black';
    }
    else {
      console.log('unknown event');
    }
  };
}
function buttonclick(e) {
  websock.send(e.id);
  console.log(e.id);
}
</script>
</head>
<body onload="javascript:start();">
<h1>Kitch Lights</h1>
<div id="ledstatus"><b>LED</b></div>
<button id="weather"  type="button" onclick="buttonclick(this);">Weather</button> 
<button id="white" type="button" onclick="buttonclick(this);">White Light</button>
<button id="party" type="button" onclick="buttonclick(this);">Have a Seizure</button>
</body>
</html>
)rawliteral";
